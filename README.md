# nuxeo-verification


１）  
RegistrationKeyを取得するため下記トライアルを登録  
※英数字しか登録に使えないので、Google連携する場合に名前が日本語だとエラーになる  

https://connect.nuxeo.com/register/

登録後にプロジェクトでCLIDを発行  

https://connect.nuxeo.com/



２）  
.envにプロジェクト名とCLIDを設定  
```
STUDIO_PROJECT_NAME=ns-example-*****
JAVA_OPTS=
CLID=****
```

３）  
登録が終わったらコンテナ立ち上げ  
```
docker-compouse up
```

立ち上げが終わると「Existing Instances」の「Technical Identifier」にデータが登録される  

４）  
立ち上がったらアクセスしてログインする  
※virtualboxを使ってる場合はlocalhostではなくIP指定  

http://localhost:8080/

ID  : Administrator  
Pass: Administrator  

ドキュメント  
https://doc.nuxeo.com/nxdoc/web-ui/


nuxeo-web-ui  
コンテンツの作成、ファイルのアップロード、参照、検索、お気に入りなどができる。
